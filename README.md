Ægir's public website
=====================

This website is hosted on GitLab Pages.


DNS settings
------------

As per the GitLab Pages
[documentation](https://docs.gitlab.com/ce/user/project/pages/), we need to
create CNAME records pointing to `aegir.gitlab.io`. Alternatively, A records
pointing towards `52.167.214.135` might work better.


HTTPS certificate
-----------------

GitLab now supports [automated LetsEncrypt certificates](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996).
Edit on [the settings page](https://gitlab.com/aegir/www.aegirproject.org/pages/domains/www.aegirproject.org)

Development
----

The site is generated using [Hugo](http://gohugo.io/).

You can install it yourself for local testing (the version in Ubuntu 16.04 is not fuctional).
`hugo server` will by default open http://localhost:1313 for you to visit.

Be sure to clone the git submodule from this project, which has the theme.

Automated build results can be found on https://gitlab.com/aegir/www.aegirproject.org/pipelines
