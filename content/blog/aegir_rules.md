---
categories:
- blog
date: 2017-12-20
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps" ]
author: Herman van Rink
username: helmo
title: "Ægir Rules"
---

Aegir faithfully runs a lot of tasks for us, but sometimes one can fail for various reasons.

That could be as simple as a filled up disk or a remote Git server that's unavailable.
<img src="/img/blog/task_error.png" alt="" style="float: right;" />

When you're logged in, you can quickly see what's going wrong.

But with more and more automation, there are days when you're not watching the Aegir dashboard of your server.

It could run a Git pull task because someone committed new code for the site you're hosting, or a Let's Encrypt certificate is being renewed. In these cases it's nice to get a notification of this. Drupal Rules module to the rescue...

The contributed [aegir_rules module](https://www.drupal.org/project/aegir_rules) integrates Rules events about the things Aegir does. You can install it like any other Drupal module in your hostmaster site, and enable it together with the rules_admin module.

In the [README](http://cgit.drupalcode.org/aegir_rules/tree/README.txt) file, we have documented an example rule that can easily be imported.
<img src="https://www.drupal.org/files/project-images/rules-140.png" alt="Rules logo" style="float: left; margin: 1em" />

The snippet below will sent an email to the hostmaster admin whenever a task fails. This can be imported on the /admin/config/workflow/rules/reaction/import page. (N.B. It needs the rules_admin sub module enabled.)

```json
{ "rules_mail_about_failed_task" : {
    "LABEL" : "Mail admin about failed task",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "hosting task" ],
    "REQUIRES" : [ "aegir_rules", "rules" ],
    "ON" : { "post_hosting_default_execute" : [] },
    "IF" : [
      { "aegir_rules_condition_task_status_failure" : { "task" : [ "task" ] } }
    ],
    "DO" : [
      { "mail" : {
          "to" : "[site:mail]",
          "subject" : "Task failed: [task:title]",
          "message" : "I\u0027m sorry to report that a task failed.\r\n\r\n[task:title]\r\n\r\n\r\nVisit this url for the task logs:\r\n[task:url]",
          "language" : [ "" ]
        }
      }
    ]
  }
}
```

You can add more conditions or actions as with any Rule.

![screenshot of the Rules admin page](/img/blog/rules_config.png)

If you have created a nice rule, please share it in the [issue queue](https://www.drupal.org/project/issues/aegir_rules?categories=All) so that others can benefit.

Who'll be the first to light up a Chrismas tree after a successful task?
