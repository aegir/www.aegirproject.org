---
categories:
- blog
date: 2018-09-17
tags: [ "Aegir", "Open source", "Drupal Planet", "Fun" ]
author: Herman van Rink
username: helmo
title: "These are the people in your Drupalverse..."
---

In honor of [DrupalEurope](https://www.drupaleurope.org/) and all the earlier DrupalCon's we've thrown together a quick Drupal 8 site that tracks all the songs covered in the DrupalCon prenote! sessions.
<img src="/img/blog/logo-drupalsongs.png" alt="Drupal songs logo" style="float: right;" />

Thanks to all those who came to the stage to wake us up before the Driesnotes.

Come and sing along at [DrupalSongs.org](https://drupalsongs.org/).



