---
categories:
- blog
date: 2017-12-30
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps" ]
author: Herman van Rink
username: helmo
title: "Helmo's year of Aegir 2017"
---



### What have I done?

It turns out a lot of Aegir.
[Anarcat](https://anarc.at/blog/) inspired be to write about the time I've spent. And now that the Aegir project has a proper blog ... why not.

190+ hours of community Aegir time (23 full 8 hour days) as per my [hamster](https://github.com/projecthamster/).

According to [Drupal.org](https://www.drupal.org/u/helmo) "Credited on 61 issues fixed in the past 1 year"


Within Aegir I worked all over the place:

- Manage releases on Aegir 3.x ( 5 in 2017, and 2 minor releases of just provision)
- Follow the "needs review" queue on D.o
- Manage Jenkins/Travis/Gitlab-CI testing infrastructure
- Try to answer support requests, issue queue's, [StackExchange](https://drupal.stackexchange.com/questions/tagged/aegir) or [IRC/Matrix](https://riot.im/app/#/room/#drupal-aegir:matrix.org)
- Weekly IRC/Matrix meeting
- Fix random issues that annoy me

With every release I try to capture more steps in a script, which has made it easier over the years. What makes a release still time consuming is the review round before and afterwards.
I look through the "needs review" queue to see if there's anything I can push on to get it ready. This is on of those rabbit holes that easily can distract you for a day.

Some of the focus was on:

- [Hosting https](https://www.drupal.org/project/hosting_https)
- [Hosting Git](https://www.drupal.org/project/hosting_git)
- [Aegir rules](https://www.drupal.org/project/aegir_rules)


### In perspective

In 2016 I spent 217 hours (27 full 8 hour days) on Aegir which is a bit more then this year.
I've had to prioritize on paying projects. As much as I love working on Aegir and other FOSS projects they generally don't pay the bills.
Most of my income comes from hosting and maintaining servers for clients.

I'm very grateful though for the monthly support I get from [Omega8cc](http://www.omega8.cc/), that gives me the time to work on the plumbing behind Aegir, e.g. automated testing and doing releases.

These are the things that personally add less value... the code is there and I know it well enough to apply a patch I like in production, doing a release makes sure all of you can use it.




### Other community things I've worked on and played with

- [DenHornOnline](http://denhornonline.nl/), a community driven [Wireless Internet Service Provider](https://en.wikipedia.org/wiki/Wireless_Internet_service_provider), maintaining over 11 km of wireless backbone connection to ~100 households..
- [Dehydrated](https://github.com/lukas2511/dehydrated) the Lets Encrypt client used by Aegir
- [CMS Scanner](https://github.com/CMS-Garden/cmsscanner), to identify which cms's are running on a server.
- [DSMR Reader](https://github.com/dennissiemensma/dsmr-reader), to get an insight in my energy usage.
- [Piwigo](https://github.com/Piwigo/Piwigo), a php photo gallery
- Portable [PoE](https://en.wikipedia.org/wiki/Power_over_Ethernet) adapter from old laptop batteries. [demo image](https://www.initfour.nl/drupal/sites/www.initfour.nl/files/pictures/portable_PoE.jpg)
- [K-9 mail](https://github.com/k9mail/k-9), my favourite mobile mail client.
- [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce)
- And more ...


[Donations validate more time being spent on Aegir](/#support). Or [hire me](https://www.initfour.nl/contact) to do more of this...

