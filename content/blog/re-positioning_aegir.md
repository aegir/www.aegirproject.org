---
categories:
- blog
date: 2017-12-08T11:39:09-05:00
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps", "Cloud Hosting / SaaS" ]
author: Christopher Gervais
username: ergonlogic
title: "Re-positioning Ægir"
---

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/KxMx6sX2yjQ?rel=0&amp;start=878" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen style="float: right; margin: 0 8em; min-width: 560;"></iframe>

Last week, Colan and I attended [SaasNorth](http://saasnorth.com/), a conference for Software-as-a-Service founders and investors, with a strong focus on marketing, finance and growth. To be honest, we were a little skeptical at the near total absence of technical content.

However, we were pleasantly surprised by some of the truly excellent speakers we heard. The one that had the most impact on us was April Dunford's [OBVIOUSLY AWESOME – Using context to move your customers from “What?” TO “Wow!”](http://saasnorth.com/sessions/go-to-market/).

While I *highly* recommend watching her whole talk, for the purposes of the rest of this post, just watch the first couple minutes (starting at 14:38). I'll wait... No really. A lot of what follows may not make sense if you aren't familiar with the marketing concept of ["positioning"](https://en.wikipedia.org/wiki/Positioning_(marketing)).

So now that you know when "cake" isn't really a cake...


Is Aegir really a "hosting system"?
-----------------------------------

While it may seem silly at first, I believe that we need to really think about this question deeply.

We regularly get feature requests for things that just don't really make sense in the context of Ægir's architecture. The ability to manage email comes to mind, along with provisioning databases or uploading static HTML via FTP. *Why do we get such requests?*

When we then look at how to accomplish them in our current (Aegir 3) architecture, we realize it's really a horrible mis-match. We only create databases, for example, in the context of installing Drupal sites. There's no stand-alone "database" entity we can re-use easily. We run into similar issues if we look at hosting static HTML sites. While we do the basics of generating vhosts and reloading web servers, we don't have subsystems to configure (nevermind secure) FTP. This even more true for email.

Our [Wikipedia page](https://en.wikipedia.org/wiki/Aegir_Hosting_System) categorizes Ægir as a [Web hosting control panel](https://en.wikipedia.org/wiki/Web_hosting_control_panel). To quote from the latter:

<blockquote>
  Some of the commonly available modules in most control panels:
  <ul>
    <li>Access to server logs.</li>
    <li>DNS Server</li>
    <li>Details of available and used webspace and bandwidth.</li>
    <li>Email account configuration.</li>
    <li>Maintaining FTP users’ accounts.</li>
    <li>Managing database.</li>
    <li>Visitor statistics using web log analysis software.</li>
    <li>Web-based file manager.</li>
  </ul>
</blockquote>

We don't actually do *any* of those things. So are we really doing ourselves (or anyone else, for that matter) any favours by calling ourselves a *"hosting system"*?

So what is Aegir, anyway?
-------------------------

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/rDR9nNFmhqE?rel=0&amp;start=333" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen style="float: right; margin: 0 8em; min-width: 560;"></iframe>

Up to now, Ægir has really focused on the lifecycle of instances of Drupal. From initial deployment of the code-base, provisioning a database, installing the site, and generating a vhost, through upgrades, backup and restore, to final de-commissioning and archiving.

Looking ahead, Ægir 5's GUI is built on Drupal 8 (yay!), and Drupal is still the *primary* application we'll support out-of-the-box. But we'll soon be able to do so much more! So it's all the more relevant to figure out what we really want Ægir to be.

Along similar lines, we need to figure out what we want it *not* to be. Implementing the hosting system features we listed above actually becomes pretty easy with Ægir 5. The real question is: *should we?*

Steli Efti's keynote at SaaSNorth may contain some insights: [Learning to say NO!](http://saasnorth.com/sessions/closing-keynote-day-1/).


SaaS Delivery Framework
-----------------------

Perhaps the biggest challenge is that hosting systems are pretty much commodities. Ægir provides more value than the $5/month price-point for a generic hosting control panel. Re-positioning the project to communicate that value is therefore important.

So I'd like to propose the term *"SaaS Delivery Framework"*. It's accurate, in so far as it describes what Ægir 5 *actually is*. Equally important, though, is that it shouldn't evoke pre-conceived notions among those hearing about it for the first time.

This might not be immediately obvious, if you aren't familiar with the proposed [Architecture](https://gitlab.com/aegir/aegir/wikis/architecture) we're working on. I'll be posting more about Ægir 5 in the coming weeks; at which point, I believe it will become clear why inventing a new category makes sense.

To that end, I justed posted a new issue on GitLab: [Figure out how to categorize Ægir 5](https://gitlab.com/aegir/aegir/issues/11). Let's take up discussion there.




