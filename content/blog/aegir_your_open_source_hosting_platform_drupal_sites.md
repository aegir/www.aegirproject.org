---
categories:
- blog
date: 2017-12-07T16:15:55-05:00
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps", "Cloud Hosting / SaaS" ]
author: "Colan Schwartz"
username: colan
title: "Aegir: Your open-source hosting platform for Drupal sites"
---

If you need an open-source solution for hosting and managing Drupal sites, there's only one option: the Aegir Hosting System. While it's possible to [find a company that will host Drupal sites for you](https://colan.consulting/blog/drupal-specific-hosting-choose-provider-those-offering-comprehensive-platforms), Aegir helps you maintain control whether you want to use your own infrastructure or manage your own [software-as-a-service (SaaS)](https://en.wikipedia.org/wiki/Software_as_a_service) product. Plus, you get all the [benefits of open source](https://opensource.com/article/17/8/enterprise-open-source-advantages).

Aegir turns ten (10) today. The first commit occurred on December 7th, 2007. We've actually produced [a timeline](http://tenyears.aegirproject.org/) including all major historical events. While Aegir had a slow uptake (the usability wasn't great in the early days), it's now being used by all kinds of organizations, [including NASA](https://www.youtube.com/watch?v=vsAOjP5iIhQ).

I got involved in the project a couple of years ago, when I needed a hosting solution for a project I was working on. I started by improving [the documentation](http://docs.aegirproject.org/), working on [contributed modules](http://docs.aegirproject.org/extend/contrib/), and then eventually [the core system](https://www.drupal.org/project/hostmaster). I've been using it ever since for all of my SaaS projects, and have been taking the lead on Drupal 8 e-commerce integration. I became [a core maintainer](http://docs.aegirproject.org/community/core-team/) of the project about a year and a half ago.

So what's new with the project? We've got several initiatives on the go. While Aegir 3 is stable and usable now ([Download it!](http://www.aegirproject.org/#download)), we've started moving away from [Drush](https://github.com/drush-ops/drush), which traditionally handles the heavy lifting (see [Provision: Drupal 8.4 support](https://www.drupal.org/project/provision/issues/2911855) for details), and into a couple of different directions. We've got an [Aegir 4 branch](https://www.drupal.org/node/2912579) based on [Symfony](https://en.wikipedia.org/wiki/Symfony), which is also included in Drupal core. This is intended to be a medium-term solution until Aegir 5 (codenamed AegirNG), a complete rewrite for hosting any application, is ready. Neither of these initiatives are stable yet, but development is on-going. Feel free to peruse [the AegirNG architecture document](https://gitlab.com/aegir/aegir/wikis/architecture), which is publicly available.

Please watch this space for future articles on the subject. I plan on writing about the following Aegir-related topics:

* Managing your development workflow across Aegir environments
* Automatic HTTPS-enabled sites with Aegir
* Remote site management with Aegir Services
* Preventing clients from changing Aegir site configurations

Happy Birthday Aegir! It's been a great ten years.

This article, [Aegir: Your open-source hosting platform for Drupal sites](https://colan.consulting/blog/aegir-your-open-source-hosting-platform-drupal-sites), appeared first on the [Colan Schwartz Consulting Services blog](https://colan.consulting/).
