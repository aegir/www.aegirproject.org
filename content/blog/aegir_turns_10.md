---
categories:
- blog
date: 2017-12-07T13:49:36-05:00
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps", "Cloud Hosting / SaaS" ]
author: Christopher Gervais
username: ergonlogic
title: Ægir Turns 10! 
---

My tenure with the Ægir Project only dates back about 7 or 8 years. I can't speak first-hand about its inception and those early days. So, I'll leave that to some of the previous core team members, many of whom are publishing blog posts of their own. I'll try to maintain an up-to-date list of links to blog posts about Ægir's 10-year anniversary here:

* [Aegir is ten!](https://www.computerminds.co.uk/drupal-code/aegir-ten) from Steven Jones at ComputerMinds.
* [Aegir: Your open-source hosting platform for Drupal sites](https://colan.consulting/blog/aegir-your-open-source-hosting-platform-drupal-sites) from Colan Schwartz of Colan Schwartz Consulting.

If I've missed any, please let me know.


New look for *www.aegirproject.org*
-----------------------------------

As part of the run-up to Ægir's 10-year anniversary, we built a new site for the project, which we released today. It will hopefully do more justice to Ægir's capabilities. In addition to the re-vamped design, we added this blog section, to make it easier for the core team to communicate with the community. So keep an eye on this space for more news in the coming weeks.


Ægir has come a long way in 10 years
------------------------------------

When I first tried to use Ægir (way back at version 0.3), I couldn't even get all the way through the installation. Luckily, I happened to live in Montréal, not too far from Koumbit, which, at the time, was a hub of Ægir development. I dropped in and introduced myself to Antoine Beaupré; then one of Ægir's lead developers.

One of his first questions was how far into the installation process I'd reached. As it turns out, he frequently asked this question when approached about Ægir. It helped him gauge how serious poeple were about running it. Back then, you pretty much needed to be a sysadmin to effectively operate it.

A few months, Antoine had wrapped the installation scripts in Debian packaging, making installation a breeze. By that point I was hooked.


Free Software is a core value
-----------------------------

Fast forward a couple years to DrupalCon Chicago. This was a time of upheaval in the Drupal community, as Drupal 7.0 was on the cusp of release, and Development Seed had announced their intention to leave the Drupal community altogether. This had far-reaching consequences for Ægir, since Development Seed had been the primary company sponsoring development, and employing project founder and lead developer Adrian Rossouw.

While in Chicago I met with Eric Gundersen, CEO of DevSeed, to talk about the future of Ægir. Whereas DevSeed had sold their flagship Drupal product, OpenAtrium, to another company, Eric was very clear that they wanted to pass on stewardship of Ægir to Koumbit, due in large part to our dedication to Free Software, and deep systems-level knowledge.


Community contributions are key
-------------------------------

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/HtSO4305ONI?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen style="float: right; margin: 0 8em; min-width: 560;"></iframe>

Since then Ægir has grown a lot. Here is one of the more interesting insights from OpenHub's [Ægir page](https://www.openhub.net/p/aegirproject):

<blockquote>
<h3>Very large, active development team</h3>
Over the past twelve months, 35 developers contributed new code to Aegir Hosting System. This is one of the largest open-source teams in the world, and is in the top 2% of all project teams on Open Hub.
</blockquote>


To help visualize all these contributions, we produced a video using [Gource](https://github.com/acaudwell/Gource). It represents the efforts of no less than 136 developers over the past 10 years. It spans the various components of Aegir Core, along with "Golden" contrib and other major sub-systems.

Of course, many other community members have contributed in other ways over the year. These include (but aren't limited to) filing bug reports and testing patches, improving our documentation, answering other users' questions on IRC, and giving presentations at local meetups.


The Future
----------

The core team has been discussing options for re-architecting Ægir to modernize the codebase and address some structural issues. In the past few months, this activity has heated up. In order to simultaneously ensure ongoing maintenance of our stable product, and to simplify innovation of future ones, the core team decided to divide responsibilities across 3 branch maintainers.

Herman van Rink (helmo) has taken up maintenance of our stable 3.x branch. He's handled the majority of release engineering for the project for the past couple years, so we can be very confident in the ongoing stability and quality of Ægir 3.

Jon Pugh, on the other hand, has adopted the 4.x branch, with the primary goal of de-coupling Ægir from Drush. This is driven largely by upstream decisions (in both Drupal and Drush) that would make continuing with our current approach increasingly difficult. He has made significant progress on porting Provision (Ægir's back-end) to Symfony. Keep an eye out for further news on that front.

For my part, I'm pursuing a more radical departure from our current architecture, re-writing Ægir from scratch atop Drupal 8 and Ansible with a full-featured (Celery/RabbitMQ) task queue in between. This promises to make Ægir significantly more flexible, which is being borne out in recent progress on the new system. While Ægir 5 will be a completely new code-base, the most of the workflows, security model and default interface will be familiar. Once it has proven itself, we can start pursuing other exciting options, like Kubernetes and OpenStack support.

So, with 10 years behind us, the future certainly looks Bryght**\***.

<br />
<br />
<br />


**\*** Bryght was the company where Adrian Rossouw began work on "hostmaster" that would eventually become the Ægir Hosting System.


