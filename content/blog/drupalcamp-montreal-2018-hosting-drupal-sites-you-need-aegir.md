---
categories:
- blog
date: 2018-06-22T14:48:45-04:00
tags: [ "Aegir", "Presentations", "Open source", "Drupal Planet", "DevOps", "Cloud Hosting / SaaS" ]
author: "Colan Schwartz"
username: colan
title: "DrupalCamp Montreal 2018: Hosting Drupal Sites? You need Aegir!"
---

On Friday, June 15<sup>th</sup>, [Christopher Gervais](https://www.drupal.org/u/ergonlogic) and I presented at [DrupalCamp Montreal 2018](https://drupalcampmontreal.com/). That's the annual gathering of the Drupal community in Montreal, Canada.

Session information:

> Do you need a self-hosted solution for hosting and managing Drupal sites? Would you like be able able to upgrade all of your sites at once with a single button click? Are you tired of dealing with all of the proprietary Drupal hosting providers that won't let you customize your set-up? Wouldn't it be nice if all of your sites could automatically get free HTTPS certificates?
> 
> If you said yes to any of the above questions, there's only one option: the [Aegir Hosting System](http://www.aegirproject.org/). While it's possible to [find a company that will host Drupal sites for you](/blog/drupal-specific-hosting-choose-provider-those-offering-comprehensive-platforms), Aegir helps you maintain control whether you want to use your own infrastructure or manage your own [software-as-a-service (SaaS)](https://en.wikipedia.org/wiki/Software_as_a_service) product. Plus, you get all the [benefits of open source](https://opensource.com/article/17/8/enterprise-open-source-advantages).
> 
> We'll cover:
> 
> *   History
> *   Architecture
> *   Basic features
> *   Advanced features
> *   Development workflows
> *   Recent additions
> *   Future
> *   Questions & discussion

A one-hour video recording of our presentation is available [on YouTube](https://www.youtube.com/watch?v=_-fJupSTKa4), and our slides (with clickable links) are [available on the original blog post mentioned below].

_This article, [DrupalCamp Montreal 2018: Hosting Drupal Sites? You need Aegir!](https://colan.consulting/blog/drupalcamp-montreal-2018-hosting-drupal-sites-you-need-aegir), appeared first on the [Colan Schwartz Consulting Services blog](https://colan.consulting/)._
