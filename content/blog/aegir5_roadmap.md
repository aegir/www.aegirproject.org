---
categories:
- blog
date: 2023-10-18
tags: [ "Aegir", "Open source", "Drupal Planet", "Aegir5" ]
author: Derek Laventure
username: spiderman
title: "Aegir5 Roadmap is moving along!"
---

You may have heard that Aegir5 is being [re-activated by the Consensus
team](https://consensus.enterprises/blog/aegir5-roadmap-update/), and this week
we published an [update to the Aegir5 docs site](https://docs.aegir.hosting/background/roadmap/releases/) describing the
User Stories we've identified, broken out into a series of Releases.

Thematically, we're looking at a progression like this:

* [Release 1](https://gitlab.com/groups/aegir/-/epics/1): Establish a pluggable backend mechanism to support stateless project hosting on either VM or Kubernetes Deployment Targets.
* [Release 2](https://gitlab.com/groups/aegir/-/epics/2): Build out stateful project support, specifically targeting Drupal
* [Release 3](https://gitlab.com/groups/aegir/-/epics/3): Add lifecycle support for importing, updating, backing up and restoring projects.
* [Release 4](https://gitlab.com/groups/aegir/-/epics/4): Introduce Application workflows for versioned releases and production environments.

We still need to add enough detail to these to be able to estimate these
realistically, in order to construct a coherent roadmap including level of effort
and timeline. However, we are hoping to move through these first few releases
fairly quickly, with the medium-term goal of making Aegir5 a viable replacement
for Aegir3.  We have created an initial set of [RFC
Issues](https://gitlab.com/aegir/aegir/-/issues?label_name[]=RFC)
https://gitlab.com/groups/aegir/-/epics?label_name[]=RFC to request feedback on
this emerging plan of attack.

If you are an Aegir 3 user or an interested Aegir 5 contributor, we'd really
appreciate hearing from you! Do the User Stories described address your desired
use-case? What's critical to you that we might be missing? We prefer to apply
our limited resources to the most valuable features and functions, so please
tell us what you value :)

As an aside, we are also wrestling with
[naming](https://gitlab.com/aegir/aegir/-/issues/139) and
[categorizing](https://gitlab.com/aegir/aegir/-/issues/11) Aegir 5, and welcome
community feedback on these questions as well.

As next steps, we plan to break out the User Stories into more detailed RFC
issues, and ultimately estimate the effort involved in implementing them. From
here we plan to seek funding either internally or through partners in the
community to execute on that plan. Please join us in making Aegir 5 a reality!
