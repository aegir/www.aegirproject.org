---
categories:
- blog
date: 2018-07-24
tags: [ "Aegir", "Open source", "Drupal Planet", "DevOps", Funding ]
author: Herman van Rink
username: helmo
title: "Back Your Stack"
---

The [Open Collective](https://opencollective.com/) project has started a nice initiative called [BackYourStack](https://backyourstack.com/).

Their original angle is to look at code from a dependency manager and derive on which libraries you're building. Their first code is for npm, hopelyfully more is soon to come. But I'd like to extend that to the Aegir community to better understand what we are using.
<img src="/img/blog/logo-bys-homepage.png" alt="" style="float: right;" />

See https://backyourstack.com/

Like Aegir they try to work out in the open ... anyone can help on https://github.com/opencollective/backyourstack



### What does Aegir build on?

It's a lot!

- Drupal core (+ all that it uses)
- A number of Drupal modules (admin_menu, betterlogin, ctools, entity, libraries, module_filter, openidadmin, overlay_paths, r4032login, tfa, tfa_basic, timeago, views, views_bulk_operations)
- Drush
- Apache or Nginx
- MySQL or MariaDB
- Debian or Ubuntu
- Git
- Composer
- GNU
- Linux
- Bash
- tar
- GitLab / GitHub
- ... (and a lot more)



It's a long list. We're really happy to be able to stand on such strong shoulders. Thanks to anyone who contributed there.

Being in an ecosystem idealy also means helping out lower in your stack.
We can't dream to all work on or even support all parts of the stack we use. But this list helped me appreciate yet a bit more what we all get from the Free software community.

What does your stack look like?

Where does Aegir fit in.... Aegir is in need of your support to [keep things going](/#donate).

Besides other donation methods we have recently added a page on [Open Collective](https://opencollective.com/aegir/)
